#!/usr/bin/env python3
# coding=utf-8


from distutils.core import setup

from configparser import ConfigParser

from os import path

from pip.req import parse_requirements

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# parse_requirements() returns generator of pip.req.InstallRequirement objects
# install_reqs = parse_requirements(path.join(path.dirname(__file__), 'REQUIREMENTS'))

if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf/drivers/usbtmc/version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(name='silf-backend-driver-usbtmc',
          description="Engine for ",
          version=cp['VERSION']['version'],
          packages=[
              'silf.drivers.usbtmc',
              ],
          package_data = {
              'silf.drivers.usbtmc': ['version.ini']
          },
          #install_requires = [str(ir.req) for ir in install_reqs]
    )
