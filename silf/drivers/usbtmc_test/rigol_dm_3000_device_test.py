
import unittest

from silf.drivers.usbtmc.rigol_dm_3000_device import *
from silf.backend.commons.util.config import prepend_current_dir_to_config_file


class TestRigolDM3000UsbtmcDevice(unittest.TestCase):

    def test_device_working(self):
        self.device = RigolDM3000UsbtmcDevice("RigolVoltage", prepend_current_dir_to_config_file("conf.ini"))
        self.device.pre_power_up_diagnostics()
        self.device.power_up()
        self.device.apply_settings({})
        self.device.start()
        print("pomiar singleDCVoltage: " + str(self.device.pop_results()))
        self.device.stop()
        self.device.power_down()

    def test_device_working_current(self):
        self.device = RigolDM3000UsbtmcDevice("RigolCurrent", prepend_current_dir_to_config_file("conf_current.ini"))
        self.device.pre_power_up_diagnostics()
        self.device.power_up()
        self.device.apply_settings({})
        self.device.start()
        print("pomiar singleDCCurrent: " + str(self.device.pop_results()))
        self.device.stop()
        self.device.power_down()

    def test_device_with_data_mode(self):
        self.device = RigolDM3000UsbtmcDevice("RigolVoltage", prepend_current_dir_to_config_file("dataDC.ini"))
        self.device.pre_power_up_diagnostics()
        #Rigol DM3058E has no dataDC mode
        if not 'DM3058E' in self.device.config[self.device.device_id]['DeviceName']:
            self.device.power_up()
            self.device.apply_settings({})
            self.device.start()

            for i in range(0, 10):
                print("pomiar " + str(i) + ": " + str(self.device.pop_results()))

            self.device.stop()
            self.device.power_down()
            self.tearDown()

    def test_data_sum_calculation(self):
        dataDc = DataDC(None)
        result_sum = dataDc._calculate_sum('-2.45328777e-02, 2.48045033e-02,-2.45346660e-02, 2.47329741e-02,-2.45757965e-02,')
        self.assertAlmostEqual(-0.0241058628, result_sum, places=10)
