import abc
import random
import time
import sys
from silf.backend.commons.util.config import validate_mandatory_config_keys, open_configfile
from silf.backend.commons.device import *
from silf.backend.commons.io.usbtmc import *
from silf.drivers.usbtmc.rigol_dm_3000_usbtmc import *

RIGOL_MODE_KEY = "rigolMode"

WORK_MODE_MAP = {
    "singleDCVoltage": lambda dev: SingleDCVoltageWorkMode(dev),
    "singleDCCurrent": lambda dev: SingleDCCurrentWorkMode(dev),
    "dataDC": lambda dev: DataDC(dev),
    "mock": lambda dev: MockWorkMode(dev)
}


class RigolDM3000UsbtmcDeviceNC():
    device_id = "Rigol"
    live = False

    def init(self, config):
        device_id = self.device_id
        print ("Init: " +device_id)
        mandatory_config = ['DeviceFilePath', RIGOL_MODE_KEY, 'resultKeyName']
        self.config = open_configfile(config)
        if self.config is None:
            raise AssertionError("Config file required")

        print('',self.config, device_id, mandatory_config)
        validate_mandatory_config_keys(self.config, device_id, mandatory_config)
        self.resultKeyName = self.config[self.device_id]['resultKeyName']

        work_mode_name = self.config[device_id][RIGOL_MODE_KEY]

        if self.config.getboolean(device_id, "use_mock", fallback=False):
            work_mode_name = "mock"

        self.workMode = WORK_MODE_MAP[work_mode_name](self)

        self.workMode.check_config()
    
    def power_up(self):
        if not self.config.getboolean(self.device_id, "use_mock", fallback=False):
            if not os.path.exists(self.config[self.device_id]['DeviceFilePath']):
                raise DeviceException('Device file does not exists: ' + self.config[self.device_id]['DeviceFilePath'])
            self.workMode.driver = UsbTmcDriver(self.config[self.device_id]['DeviceFilePath'])
            self.workMode.driver.runCmd(BaseUsbTmcCommands.IDN)
            idnStr = self.workMode.driver.getResult()
            if not idnStr or not self.config[self.device_id]['DeviceName'] in idnStr:
                raise DeviceException('Given usbtmc file does not correspond to device name: '+idnStr)
        self.workMode.set_display_brightness()
        self.workMode.invert_display_colors()

    def apply_settings(self, settings):
        self.workMode.run_apply_settings(settings)

    def read_state(self):
        return {
            self.resultKeyName: self.workMode.get_result()
        }
    
    def power_down(self):
        self.workMode.on_power_down()


class RigolDM3000UsbtmcDevice(Device):
    MAIN_LOOP_INTERVAL = 0.3
    """
        Expected data in config file in section device_id:
            DeviceFilePath - file path to dev file which represents device
            DeviceName - name of the device, taken from /dev/usbtmc0
    """

    def __init__(self, device_id="default", config_file=None):
        super().__init__(device_id, config_file)
        validate_mandatory_config_keys(self.config,
            device_id, ("DeviceFilePath", RIGOL_MODE_KEY, "resultKeyName"))

        self.resultKeyName = self.config[self.device_id]['resultKeyName']

        work_mode_name = self.config[device_id][RIGOL_MODE_KEY]

        if work_mode_name == "mock":
            raise DeviceException(self.__MOCK_WORK_MODE_ERROR_MESSAGE)

        if self.config.getboolean(device_id, "use_mock", fallback=False):
            work_mode_name = "mock"

        self.workMode = WORK_MODE_MAP[work_mode_name](self)

        self.workMode.check_config()

    def pre_power_up_diagnostics(self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]):
        self.workMode.power_up_diagnostics(diagnostics_level)

    def apply_settings(self, settings):
        """
        co mozna ustawic:
            SET_MEASURE_MODE
            SET_DC_RESOLUTION

            SET_DC_IMPEDANCE
            SET_DC_DIGIT
            SET_DC_RANGE

        :param settings:
        :return:
        """
        #TODO mozna by zrobic ze po kazdym SET-ie uruchamiany jest GEt sprawdzajacy
        self.workMode.run_apply_settings(settings)
        super().apply_settings(settings)

    def pop_results(self):
        return [{
            self.resultKeyName: self.workMode.get_result()
        }]

    def power_up(self):
        self.workMode.on_power_up()
        super().power_up()

    def power_down(self):
        self.workMode.on_power_down()
        super().power_down()

    def _tearDown(self):
        self.power_down()
        super()._tearDown()

    __MOCK_WORK_MODE_ERROR_MESSAGE = ("You shouldn't specify work mode "
        "explicitly, via `{}` setting, rather use `use_mock` "
        "setting".format(RIGOL_MODE_KEY))


class WorkMode(metaclass = abc.ABCMeta):

    def __init__(self, rigolDevice):
        self.rigolDevice = rigolDevice
        self.driver = None
    
    def power_up_diagnostics(self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]):
        """
        sprawdz czy istnieje plik device-a
        pobierz identyfikator aby sprawdzic komunikacje

        :param diagnostics_level:
        :return:
        """
        startingState = self.rigolDevice.state
        if startingState == DEVICE_STATES[OFF]:
            if not os.path.exists(self.rigolDevice.config[self.rigolDevice.device_id]['DeviceFilePath']):
                raise InvalidStateException('Device file does not exists: ' + self.rigolDevice.config[self.rigolDevice.device_id]['DeviceFilePath'])
            self.rigolDevice.power_up()

        self.rigolDevice.workMode.driver.runCmd(BaseUsbTmcCommands.IDN)
        idnStr = self.rigolDevice.workMode.driver.getResult()
        if not idnStr or not self.rigolDevice.config[self.rigolDevice.device_id]['DeviceName'] in idnStr:
            if startingState == DEVICE_STATES[OFF]:
                self.rigolDevice.power_down()
            raise DiagnosticsException('Given usbtmc file does not correspond to device name: '+idnStr)

        if startingState == DEVICE_STATES[OFF]:
            self.rigolDevice.power_down()

    def on_power_up(self):
        self.driver = UsbTmcDriver(self.rigolDevice.config[self.rigolDevice.device_id]['DeviceFilePath'])
        self.set_display_brightness()
        self.invert_display_colors()

    def on_power_down(self):
        if self.driver is not None:
            self.invert_display_colors()
            self.driver.close()
            self.driver = None

    def invert_display_colors(self):
        if self.rigolDevice.config.getboolean(self.rigolDevice.device_id, "invert_display_color", fallback=False):
            self.driver.runCmd(RigolDM3000UsbTmcCommands.INVERT_DISPLAY_COLOR)
            contrast = self.rigolDevice.config[self.rigolDevice.device_id].get("inverted_display_contrast", "32")
            self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DISPLAY_CONTRAST, contrast)

    def set_display_brightness(self):
        brt = self.rigolDevice.config[self.rigolDevice.device_id].get("display_brightness", "22")
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DISPLAY_BRIGHTNESS, brt)

    def set_relative_measurement(self, state):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_RELATIVE_MEASUREMENT, state)

    @abc.abstractclassmethod
    def get_result(self):
        pass

    @abc.abstractclassmethod
    def run_apply_settings(self, settings):
        pass

    @abc.abstractclassmethod
    def check_config(self):
        pass


class SingleDCVoltageWorkMode(WorkMode):

    def __init__(self, rigolDevice):
        super().__init__(rigolDevice)

    def check_config(self):
        if self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY] != "singleDCVoltage":
            raise ValueError("Wrong WorkMode for conf value "
                             + self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY])

    def run_apply_settings(self, settings):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_FUNCTION_VOLTAGE_DC)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_MEASURE_MODE, SetMeasureModeParams.AUTO)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DC_VOLTAGE_RESOLUTION, SetDcResolutionParams.MAX)

    def get_result(self):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.GET_DC_VOLTAGE)
        strVal = self.driver.getResult()
        floVal = float(strVal)
        print("RIGOL GET VALUE: "+str(floVal), file=sys.stderr)
        return floVal


class SingleDCCurrentWorkMode(WorkMode):

    def __init__(self, rigolDevice):
        super().__init__(rigolDevice)

    def check_config(self):
        if self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY] != "singleDCCurrent":
            raise ValueError("Wrong WorkMode for conf value "
                             + self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY])

    def run_apply_settings(self, settings):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_FUNCTION_CURRENT_DC)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_MEASURE_MODE, SetMeasureModeParams.AUTO)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DC_CURRENT_RESOLUTION, SetDcResolutionParams.MAX)

    def get_result(self):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.GET_DC_CURRENT)
        strVal = self.driver.getResult()
        floVal = float(strVal)
        print("RIGOL GET VALUE: "+str(floVal), file=sys.stderr)
        return floVal


class DataDC(WorkMode):

    def __init__(self, rigolDevice):
        super().__init__(rigolDevice)

    def check_config(self):
        if self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY] != "dataDC":
            raise ValueError("Wrong WorkMode for conf value "
                             + self.rigolDevice.config[self.rigolDevice.device_id][RIGOL_MODE_KEY])
        self.rigolDevice.config.validate_mandatory_config_keys(self.rigolDevice.device_id,
                                                               ("dataFunction", "dataFunctionRange", "dataAcquisitionRate"))
        self.dataFunction = SetDataLogFunctionParams.from_string(
            self.rigolDevice.config[self.rigolDevice.device_id]["dataFunction"])
        self.dataFunctionRange = self.rigolDevice.config[self.rigolDevice.device_id]["dataFunctionRange"]
        self.dataAcquisitionRate = SetDataAcquisitionRateParams.from_string(
            self.rigolDevice.config[self.rigolDevice.device_id]["dataAcquisitionRate"])
        self.dataStopNumber = SetDataAcquisitionRateParams.get_stop_number(self.dataAcquisitionRate)

    def run_apply_settings(self, settings):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DATALOG_FUNCTION, self.dataFunction, self.dataFunctionRange)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DATA_ACQUISITION_RATE, self.dataAcquisitionRate)
        self.driver.runCmd(RigolDM3000UsbTmcCommands.SET_DATA_ACQUISITION_STOP_NUMBER, self.dataStopNumber)

    def get_result(self):
        self.driver.runCmd(RigolDM3000UsbTmcCommands.RUN_DATA_ACQUISITION)
        is_running = 'RUN'
        counter = 0
        while is_running != 'STOP':
            time.sleep(0.1)
            self.driver.runCmd(RigolDM3000UsbTmcCommands.IS_DATA_ACQUISITION_RUNNING)
            is_running = self.driver.getResult()
            # TODO wczytywanie z konfiguracji ilosci powtorzen i sleep-a powyzej
            counter += 1
            if counter > 20:
                raise DeviceException("Can not get data")
        self.driver.runCmd(RigolDM3000UsbTmcCommands.GET_DATA_ACQUISITION_DATA, 1, self.dataStopNumber)
        str_data = self.driver.getResult()
        return self._calculate_sum(str_data) / self.dataStopNumber

    def _calculate_sum(self, str_data):
        # calculate avg for given data, format is float numbers divided by ,
        st = end = sum = 0
        for c in str_data:
            if c == ',':
                sum += float(str_data[st:end])
                st = end + 1
            end += 1
        return sum


class MockWorkMode(WorkMode):
    def power_up_diagnostics(self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]):
        pass

    def run_apply_settings(self, settings):
        pass

    def on_power_up(self):
        pass

    def on_power_down(self):
        pass

    def get_result(self):
        time.sleep(2)
        return random.randint(0, 100)

    def check_config(self):
        pass

    def invert_display_colors(self):
        pass

    def set_display_brightness(self):
        pass

    def set_relative_measurement(self, state):
        pass