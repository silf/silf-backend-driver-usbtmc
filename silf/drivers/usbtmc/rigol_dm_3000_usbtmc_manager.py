# coding=utf-8

from silf.backend.commons.device_manager import *
from silf.drivers.usbtmc import RigolDM3000UsbtmcDevice


class RigolDM3000UsbtmcManager(SingleModeDeviceManager):

    USE_WORKER = True

    CONTROLS = None

    OUTPUT_FIELDS = None

    DEVICE_ID = "rigol"

    DEVICE_CONSTRUCTOR = RigolDM3000UsbtmcDevice

    RESULT_CREATORS = [
        ReturnLastElementResultCreator("voltage")
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.unit =  int(self.config[self.DEVICE_ID].get("unit","1"))
        self.precision = int(self.config[self.DEVICE_ID].get("precision","3"))
    
    def _convert_result(self, dict):

        if 'voltage' in dict:
            dict['voltage'] = round(self.unit * float(dict['voltage']),self.precision)
        return super()._convert_result(dict)


class RigolDM3000UsbtmcManagerCurrent(SingleModeDeviceManager):

    USE_WORKER = True

    CONTROLS = None

    OUTPUT_FIELDS = None

    DEVICE_ID = "rigol"

    DEVICE_CONSTRUCTOR = RigolDM3000UsbtmcDevice

    RESULT_CREATORS = [
        ReturnLastElementResultCreator("current")
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.unit =  int(self.config[self.DEVICE_ID].get("unit","1"))
        self.precision = int(self.config[self.DEVICE_ID].get("precision","3"))

    def _convert_result(self, dict):

        if 'current' in dict:
            dict['current'] = round(self.unit * float(dict['current']),self.precision)
        return super()._convert_result(dict)

