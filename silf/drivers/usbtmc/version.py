# coding=utf-8

import logging
from os import path
from configparser import ConfigParser

cp = ConfigParser()
file = path.join(path.dirname(__file__), "version.ini")
with open(file) as f:
    cp.read_file(f)

__VERSION__ = cp['VERSION']['version'].split(".")

logging.getLogger(__name__).\
    info("Running {} version {}".format(__name__, ".".join(__VERSION__)))

