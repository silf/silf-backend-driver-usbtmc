
from silf.backend.commons.io.usbtmc import UsbTmcCommand


class RigolDM3000UsbTmcCommands(object):
    # The query returns the measurement function currently used by the meter such as DCV
    GET_FUNCTION = UsbTmcCommand(':FUNCtion?', resultSize=10)
    # Turns on the DC voltage measurement function.
    SET_FUNCTION_VOLTAGE_DC = UsbTmcCommand(':FUNCtion:VOLTage:DC', isResultExpected=False)
    # Turns on the DC current measurement function.
    SET_FUNCTION_CURRENT_DC = UsbTmcCommand(':FUNCtion:CURRent:DC', isResultExpected=False)
    # Turns on the ratio measurement for DC voltage measurerments.
    SET_FUNCTION_VOLTAGE_DC_RATIO = UsbTmcCommand(':FUNCtion:VOLTage:DC:RATIo', isResultExpected=False)
    # Turns on the ratio measurement for DC current measurerments.
    SET_FUNCTION_CURRENT_DC_RATIO = UsbTmcCommand(':FUNCtion:CURRent:DC:RATIo', isResultExpected=False)
    # Turns on the AC voltage measurement function.
    SET_FUNCTION_VOLTAGE_AC = UsbTmcCommand(':FUNCtion:VOLTage:AC', isResultExpected=False)
    # Turns on the AC current measurement function.
    SET_FUNCTION_CURRENT_AC = UsbTmcCommand(':FUNCtion:CURRent:AC', isResultExpected=False)
    # Sets the DC impedance to 10MΩ or >10GΩ
    SET_DC_IMPEDANCE = UsbTmcCommand(':MEASure:VOLTage:DC:IMPEdance', isResultExpected=False)
    # Sets the display digit for DC voltage measurement values
    SET_VOLTAGE_DC_DIGIT = UsbTmcCommand(':MEASure:VOLTage:DC:DIGIt', isResultExpected=False)
    # Sets the display digit for DC current measurement values
    SET_CURRENT_DC_DIGIT = UsbTmcCommand(':MEASure:CURRent:DC:DIGIt', isResultExpected=False)
    # The query returns the current DC voltage in the form of scientific notation such as +2.53021747E-04, the unit is V
    GET_DC_VOLTAGE = UsbTmcCommand(':MEASure:VOLTage:DC?', resultSize=64)
    # The query returns the DC current in the form of scientific notation such as +2.53021747E-04, the unit is A
    GET_DC_CURRENT = UsbTmcCommand(':MEASure:CURRent:DC?', resultSize=64)
    # Sets the DC voltage measurement range.
    SET_DC_VOLTAGE_RANGE = UsbTmcCommand(':MEASure:VOLTage:DC', isResultExpected=False)
    # Sets the DC current measurement range.
    SET_DC_CURRENT_RANGE = UsbTmcCommand(':MEASure:CURRent:DC', isResultExpected=False)
    # Sets the measurement mode to Auto or Manual.
    SET_MEASURE_MODE = UsbTmcCommand(':MEASure', isResultExpected=False)
    # Sets the reading resolution for DC voltage measurements
    SET_DC_VOLTAGE_RESOLUTION = UsbTmcCommand(':RESOlution:VOLTage:DC', isResultExpected=False)
    # Sets the reading resolution for DC current measurements
    SET_DC_CURRENT_RESOLUTION = UsbTmcCommand(':RESOlution:CURRent:DC', isResultExpected=False)
    # Causes the beeper buzz once.
    RUN_BEEPER = UsbTmcCommand(':SYSTem:BEEPer', isResultExpected=False)
    # Sets the measurement item that needs to acquire data and its range.
    SET_DATALOG_FUNCTION = UsbTmcCommand(':DATAlog:CONFigure:FUNCtion', isResultExpected=False)
    # Sets the sample rate for data acquisitions.
    SET_DATA_ACQUISITION_RATE = UsbTmcCommand(':DATAlog:CONFigure:RATE', isResultExpected=False)
    # Sets the number for data acquisitions. Integer ranging from 1 to 2097151
    SET_DATA_ACQUISITION_STOP_NUMBER = UsbTmcCommand(':DATAlog:CONFigure:STOPmode:NUMber', isResultExpected=False)
    # Executes the configured data acquisitions.
    RUN_DATA_ACQUISITION = UsbTmcCommand(':DATAlog:RUN', isResultExpected=False)
    # Queries if the meter is running under the data acquisition. The query returns RUN or STOP.
    IS_DATA_ACQUISITION_RUNNING = UsbTmcCommand(':DATAlog:RUN?', resultSize=5)
    # The query returns the acquired data
    GET_DATA_ACQUISITION_DATA = UsbTmcCommand(':DATAlog:DATA?', resultSize=5*1024)
    # Inverts the display color
    INVERT_DISPLAY_COLOR = UsbTmcCommand(':SYSTem:DISPlay:INVErt', isResultExpected=False)
    # Sets the screen contrast. <value> is an integer ranging from 0 to 32.
    SET_DISPLAY_CONTRAST = UsbTmcCommand(':SYSTem:DISPlay:CONTrast', isResultExpected=False)
    # Sets the screen brightness. <value> is an integer ranging from 0 to 32.
    SET_DISPLAY_BRIGHTNESS = UsbTmcCommand(':SYSTem:DISPlay:BRIGht', isResultExpected=False)
    # Set Relative measurement.
    SET_RELATIVE_MEASUREMENT = UsbTmcCommand(':CALCulate:REL:STATe', isResultExpected=False)

# odczyt napiecia - DC (direct current) = staly prad  = :MEASure:VOLTage:DC?
# ustawienie dokladnosci = :RESOlution:VOLTage:DC
# ustawienie zakresu pomiaru napiec = :MEASure:VOLTage:DC
# usrednianie pomiaru -Na pozniej


class SetImpedanceParams(object):
    IMP_10M = '10M'
    IMP_10G = '10G'


class SetDcDigitParams(object):
    DIGIT_5 = '5'
    DIGIT_6 = '6'
    DIGIT_7 = '7'


class SetDcRangeParams(object):
    TO_200mV = '0'
    TO_2v = '1'
    TO_20V = '2'
    TO_200V = '3'
    TO_1000V = '4'
    MIN = 'MIN'
    MAX = 'MAX'
    DEF = 'DEF'


class SetMeasureModeParams(object):
    AUTO = 'AUTO'
    MANUAL = 'MANU'


class SetDcResolutionParams(object):
    DIGIT_4_Half = '0'
    DIGIT_5_Half = '1'
    DIGIT_6_Half = '2'
    MIN = 'MIN'
    MAX = 'MAX'
    DEF = 'DEF'


class SetDataLogFunctionParams(object):
    DCV = "DCV"
    DCI = "DCI"
    Resistance = "RESistance"
    Fresistance = "FRESistance"

    @staticmethod
    def from_string(strVal):
        if strVal == "DCV":
            return SetDataLogFunctionParams.DCV
        elif strVal == "DCI":
            return SetDataLogFunctionParams.DCI

        raise ValueError("No value defined for: " + strVal)


class SetDataAcquisitionRateParams(object):
    _1_FOR_10_MIN = "1"
    _1_FOR_5_MIN = "2"
    _1_FOR_1_MIN = "3"
    _1_FOR_10_SEC = "4"
    _1_FOR_1_SEC = "5"
    _10_FOR_1_SEC = "6"
    _50_FOR_1_SEC = "7"
    _100_FOR_1_SEC = "8"
    _833_FOR_1_SEC = "9"
    _1000_FOR_1_SEC = "10"
    _5000_FOR_1_SEC = "11"
    _10000_FOR_1_SEC = "12"
    _50000_FOR_1_SEC = "13"

    @staticmethod
    def from_string(str_val):
        if str_val == "100/s":
            return SetDataAcquisitionRateParams._100_FOR_1_SEC

        raise ValueError("No value defined for: " + str_val)

    @staticmethod
    def get_stop_number(rate_param):
        if SetDataAcquisitionRateParams._100_FOR_1_SEC == rate_param:
            return 100

        raise ValueError("No value defined for: " + str(rate_param))

